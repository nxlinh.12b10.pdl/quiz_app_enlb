
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of, tap } from 'rxjs';
import { HttpResponse } from 'src/app/pages/models/http-response';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public constructor(protected http: HttpClient) { }

  public get(url: string): Observable<HttpResponse> {
    return this.http.get<HttpResponse>(url).pipe(
      tap((response) => response),
      catchError((err) => of(err))
    );
  }
}
