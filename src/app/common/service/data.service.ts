import { Injectable } from '@angular/core';
import { Question } from 'src/app/pages/models/question';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private question: Question[] = [];

  constructor() { }

  setQuestions(question: Question[]): void {
    this.question = question;
  }

  getQuetions(): Question[] {
    return this.question;
  }
}
