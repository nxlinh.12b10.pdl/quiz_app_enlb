import {trigger, transition, state, style, query, animate} from '@angular/animations'


export const fader = trigger('routerAnimations', [
  transition('* <=> *', [
    style({ opacity: 0}),
    animate('0.9s', style({ opacity: 1})),
  ])
]);
