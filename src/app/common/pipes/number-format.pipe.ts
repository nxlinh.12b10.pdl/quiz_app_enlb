import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalRound',
})
export class DecimalRound implements PipeTransform {
  transform(value: number): number {
    return parseFloat(value.toFixed(2));
  }
}


@Pipe({
  name: 'numberRound',
})
export class NumberRound implements PipeTransform {
  transform(value: number): number {
    return Math.ceil(value);
  }
}
