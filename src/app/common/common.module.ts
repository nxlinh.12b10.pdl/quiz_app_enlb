import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DecimalRound, NumberRound } from './pipes/number-format.pipe';



@NgModule({
  declarations: [
    DecimalRound,
    NumberRound
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    MaterialModule,
    DecimalRound,
    NumberRound
  ]
})
export class AppCommonModule { }
