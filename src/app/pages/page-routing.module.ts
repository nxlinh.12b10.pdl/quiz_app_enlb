import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './page.component';
import { MainComponent } from './components/main/main.component';
import { DoQuizComponent } from './components/do-quiz/do-quiz.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ReviewResultsComponent } from './components/review-results/review-results.component';
const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      { path: '', component: MainComponent, data: { animation: 'main'}},
      { path: 'doQuiz', component: DoQuizComponent, data: { animation: 'doQuiz'}},
      { path: 'reviewQuestions', component: ReviewResultsComponent, data: { animation: 'reviewQuestions'}},
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule {}

