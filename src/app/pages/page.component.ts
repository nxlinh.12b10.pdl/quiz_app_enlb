import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { fader } from '../common/animations/router-animation';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  animations: [
    fader
  ],

})
export class PageComponent {

  public prepareRoute(outlet: RouterOutlet){
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animations'];
  }
}
