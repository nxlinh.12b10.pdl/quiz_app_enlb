export interface NotifiData {
  icon?: 'icons8-congratulation-100.png' | 'icons8-reset-100.png',
  title?: 'Congratulations!!' | 'Completed!!',
  greeting?: 'You are amazing!!' | 'Better luck next time!!'
  correctAnswer?: number,
  numberOfQuestions?: number,
  completionTime?: string
}
