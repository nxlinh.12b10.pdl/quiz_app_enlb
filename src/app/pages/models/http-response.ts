import { Question } from "./question";

export interface HttpResponse {
    data: Question[];
}
