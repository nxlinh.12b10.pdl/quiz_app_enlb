export interface Question {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  incorrect_answers: string[];
}

export interface ChoiceQuestionData {
  question?: Question,
  userChoice?: string,
  answers?: string[];
}

export interface ReviewQuestionData {
  questions: ChoiceQuestionData[],
  numberOfQuestions: number,
  numberOfCorrectQuestions: number,
  completionTime: number
}
