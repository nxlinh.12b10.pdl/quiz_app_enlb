import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { HttpResponse } from '../models/http-response';
import { HttpService } from 'src/app/common/service/http.service';
import { Question } from '../models/question';

@Injectable({
  providedIn: 'root'
})
export class QuizService extends HttpService{

  private url = 'http://localhost:3000/results';

  constructor(protected override http: HttpClient) {
    super(http);
  }


  public getAllQuestions(): Observable<Question[]> {
    return this.get(this.url).pipe(map((response: any) => response)) as Observable<Question[]>;
  }


}
