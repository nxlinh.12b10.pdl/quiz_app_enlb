import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import {
  ChoiceQuestionData,
  Question,
  ReviewQuestionData,
} from '../../models/question';
import { isEmpty } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { NotificationComponent } from '../notification/notification.component';

@Component({
  selector: 'app-do-quiz',
  templateUrl: './do-quiz.component.html',
  styleUrls: ['./do-quiz.component.scss']
})
export class DoQuizComponent implements OnInit {
  public listQuestion: Array<Question> = [];
  public currentQuestionIndex = 0;
  public answers: string[] = [];
  public isCorrect: boolean = false;
  public answerSelected: string = '';
  public correctAnswers = 0;
  public startTime!: Date;
  public endTime!: Date;
  public isFinished = false;
  public choiceQuestions: ChoiceQuestionData[] = [];
  public reviewQuestions!: ReviewQuestionData;
  public currentQuestion: Question = {
    category: '',
    type: '',
    difficulty: '',
    question: '',
    correct_answer: '',
    incorrect_answers: [],
  };

  public constructor(private router: Router, private dialog: MatDialog) {}

  public ngOnInit(): void {
    if (history.state && history.state.questions) {
      this.listQuestion = this.shuffleArray(history.state.questions);
      this.currentQuestion = this.listQuestion[this.currentQuestionIndex];
      this.getAnswers();
      this.startTime = new Date();
    }
  }

  // Choice Question Event
  public onSelect(): void {
    if (!isEmpty(this.answerSelected)) {
      if (Object.is(this.answerSelected, this.currentQuestion.correct_answer)) {
        this.isCorrect = true;
        this.correctAnswers++;
      }else {
        this.isCorrect = false;
      }

      this.choiceQuestions.push({
        question: this.currentQuestion,
        userChoice: this.answerSelected,
        answers: this.answers
      });
    }
  }

  // Next question
  public changeQuestion() {
    if (this.currentQuestionIndex < this.listQuestion.length - 1) {
      this.currentQuestionIndex++;
      this.currentQuestion = this.listQuestion[this.currentQuestionIndex];
      this.getAnswers();
      this.answerSelected = '';
    }
  }

  // Finish Quiz
  public onFinish() {
    this.endTime = new Date();
    const completionTime = this.completionTime(this.startTime, this.endTime);
    const notifiData = {
      completionTime: completionTime,
      correctAnswers: this.correctAnswers,
      numberOfQuestions: this.listQuestion.length,
    };

    this.isFinished = true;
    this.openDialog(notifiData);
  }

  // Open notification dialog
  public openDialog(data: Object) {
    const dialogRef = this.dialog.open(NotificationComponent, {
      width: '300px',
      height: '300px',
      data: data,
      disableClose: true,
      enterAnimationDuration:500,
      exitAnimationDuration: 500
    });

    dialogRef.afterClosed().subscribe((action: string) => {
      if (action === 'playAgain') {
        this.router.navigate(['/main']);
      } else {
        this.reviewQuestions = {
          questions: this.choiceQuestions,
          completionTime: this.endTime.getTime() - this.startTime.getTime(),
          numberOfCorrectQuestions: this.correctAnswers,
          numberOfQuestions: this.listQuestion.length,
        };

        const navigattionExtras: NavigationExtras = {
          state: {
            reviewQuestions: this.reviewQuestions,
          },
        };
        this.router.navigate(['/reviewQuestions'], navigattionExtras);
      }
    });
  }

  // Caculate completion time
  public completionTime(startTime: Date, endTime: Date) {
    const elapsedTime: number = endTime.getTime() - startTime.getTime();

    const hours = Math.floor(elapsedTime / (1000 * 60 * 60));
    const minutes = Math.floor((elapsedTime % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((elapsedTime % (1000 * 60)) / 1000);

    if (hours === 0 && minutes === 0 && seconds !== 0) {
      return `${seconds} seconds`;
    } else {
      return `${hours < 10 ? '0' + hours : hours}:${
        minutes < 10 ? '0' + minutes : minutes
      }:${seconds}`;
    }
  }

  // Gets list answers and shuffle random answers
  public getAnswers() {
    this.answers = this.currentQuestion.incorrect_answers;
    this.answers.push(this.currentQuestion.correct_answer);
    this.answers = this.shuffleArray(this.answers);
  }

  // Shuffle random answers
  public shuffleArray<T>(array: T[]) {
    const newArray = [...array];
    for (let i = newArray.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
    }
    return newArray;
  }
}
