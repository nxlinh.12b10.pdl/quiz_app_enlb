import { Component, OnInit } from '@angular/core';
import { ChoiceQuestionData, ReviewQuestionData } from '../../models/question';
import { Router } from '@angular/router';

@Component({
  selector: 'app-review-results',
  templateUrl: './review-results.component.html',
  styleUrls: ['./review-results.component.scss'],
})
export class ReviewResultsComponent implements OnInit {
  public reviewQuestionsData!: ReviewQuestionData;
  public choiceQuestionData!: ChoiceQuestionData[];
  public incorrectQuestions: number = 0;
  public avgTimePerQuestion: number = 0;

  public constructor(private router: Router) {}

  public ngOnInit(): void {
    if (history.state && history.state.reviewQuestions) {
      this.reviewQuestionsData = history.state.reviewQuestions;

      console.log(this.reviewQuestionsData);

      this.incorrectQuestions =
        this.reviewQuestionsData.numberOfQuestions -
        this.reviewQuestionsData.numberOfCorrectQuestions;

      this.avgTimePerQuestion =
        this.reviewQuestionsData.completionTime /
        1000 /
        this.reviewQuestionsData.numberOfQuestions;

      this.choiceQuestionData = this.reviewQuestionsData.questions;
    }
  }

  public goBackHome() {
    this.router.navigate(['/main']);
  }
}
