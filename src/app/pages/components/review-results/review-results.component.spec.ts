import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewResultsComponent } from './review-results.component';

describe('ReviewResultsComponent', () => {
  let component: ReviewResultsComponent;
  let fixture: ComponentFixture<ReviewResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviewResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
