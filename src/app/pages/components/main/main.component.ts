import { Component } from '@angular/core';
import { NavigationExtras, Router} from '@angular/router';
import { QuizService } from '../../services/quiz.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {
  public constructor(
    private router: Router,
    private quizService: QuizService
  ) {}

  public doQuiz() {
    this.quizService.getAllQuestions().subscribe(quest => {
      const navigattionExtras:  NavigationExtras = {
        state: {
          questions:quest
        }
      }
      this.router.navigate(['doQuiz'], navigattionExtras);
    })

  }
}
