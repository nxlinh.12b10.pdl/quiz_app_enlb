import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifiData } from '../../models/notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],

})
export class NotificationComponent implements OnInit {

  public notifiData: NotifiData = {};

  public constructor(
    public dialogRef: MatDialogRef<NotificationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router
  ) {}

  ngOnInit(): void {

    const correctAnswers = this.data.correctAnswers;
    const numberOfQuestions: number = this.data.numberOfQuestions;

    if(correctAnswers >= Math.ceil(numberOfQuestions*0.5)) {
      this.notifiData = {
        icon:'icons8-congratulation-100.png',
        title: 'Congratulations!!',
        greeting: 'You are amazing!!',
        correctAnswer: correctAnswers,
        numberOfQuestions: numberOfQuestions,
        completionTime: this.data.completionTime
      }
    }else {
      this.notifiData = {
        icon: 'icons8-reset-100.png',
        title: 'Completed!!',
        greeting: 'Better luck next time!!',
        correctAnswer: correctAnswers,
        numberOfQuestions: numberOfQuestions,
        completionTime: this.data.completionTime
      }
    }


  }

  public playAgain() {
    this.dialogRef.close('playAgain');
  }

  public checkResult() {
    this.dialogRef.close('checkResult');
  }
}
