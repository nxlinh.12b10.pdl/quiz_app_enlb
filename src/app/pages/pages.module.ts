import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page.component';
import { MainComponent } from './components/main/main.component';
import { PageRoutingModule } from './page-routing.module';
import { DoQuizComponent } from './components/do-quiz/do-quiz.component';
import { MaterialModule } from '../common/material/material.module';
import { QuizService } from './services/quiz.service';
import { DataService } from '../common/service/data.service';
import { FormsModule } from '@angular/forms';
import { NotificationComponent } from './components/notification/notification.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReviewResultsComponent } from './components/review-results/review-results.component';
import { AppCommonModule } from '../common/common.module';

@NgModule({
  declarations: [
    PageComponent,
    MainComponent,
    DoQuizComponent,
    NotificationComponent,
    ReviewResultsComponent,
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    MaterialModule,
    FormsModule,
    MatDialogModule,
    AppCommonModule
  ],
  providers: [
    QuizService,
    DataService
  ]
})
export class PagesModule { }
